<?php
/**
 * Fichier gérant l'installation et désinstallation du plugin Exclure secteur.
 * Le schéma de données du plugin consiste uniquement en une configuration meta.
 *
 * @plugin     Exclure secteur
 *
 * @copyright  2013
 * @author     Maïeul Rouquette
 *
 * @licence    GPL 3
 */
if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

/**
 * Fonction d'installation et de mise à jour du plugin.
 *
 * @param string $nom_meta_base_version Nom de la meta informant de la version du schéma de données du plugin installé dans SPIP
 * @param string $version_cible         Version du schéma de données dans ce plugin (déclaré dans paquet.xml)
 *
 * @return void
**/
function exclure_sect_upgrade(string $nom_meta_base_version, string $version_cible) : void {
	// Initialisation du tableau des mises à jour.
	$maj = [];

	// Initialisation des valeurs par défaut.
	$config_defaut = configurer_exclure_sect();

	// Pour la première installation du plugin
	$maj['create'] = [
		['ecrire_config', 'secteur', $config_defaut]
	];

	include_spip('base/upgrade');
	maj_plugin($nom_meta_base_version, $version_cible, $maj);
}

/**
 * Fonction de désinstallation du plugin.
 *
 * @param string $nom_meta_base_version Nom de la meta informant de la version du schéma de données du plugin installé dans SPIP
 *
 * @return void
**/
function exclure_sect_vider_tables(string $nom_meta_base_version) : void {
	// On efface la configuration spécifique du plugin
	include_spip('inc/config');
	if (lire_config('secteur')) {
		effacer_config('secteur');
	}

	// Puis la meta de version du schéma du plugin.
	effacer_meta($nom_meta_base_version);
}

/**
 * Initialise la configuration du plugin.
 *
 * @return array Le tableau de la configuration par défaut qui servira à initialiser la meta `secteur`.
**/
function configurer_exclure_sect() : array {
	$config = [
		'exclure_sect' => [],
		'tout'         => '',
		'idexplicite'  => ''
	];

	return $config;
}
