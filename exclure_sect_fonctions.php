<?php

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

if (!function_exists('critere_tout_voir_dist')) {
	/**
	 * Compile le critère `{tout_voir}` qui permet d'outrepasser l'exclusion de certains secteurs.
	 *
	 * @param string  $idb      Identifiant de la boucle
	 * @param array   $boucles  AST du squelette
	 * @param Critere $criteres Paramètres du critère dans cette boucle
	 *
	 * @return void
	 */
	function critere_tout_voir_dist(string $idb, array &$boucles, Critere $criteres) : void {
		$boucle = &$boucles[$idb];
		$boucle->modificateur['tout_voir'] = true;
	}
}
