<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/exclure_sect-paquet-xml-exclure_secteur?lang_cible=en
// ** ne pas modifier le fichier **

return [

	// E
	'exclure_sect_description' => 'This plugin automatically excludes selected root sections from the ARTICLES / SITES / RUBRIQUES / BREVES / FORUMS loops (following the SPIP rule, which only takes into account forums linked to articles).

Please note that, for the moment, the plugin doesn’t manage joins (for example, it won’t exclude forums that belong to an article in an excluded root section).

If, in a loop, you specify the root section explicitly with a <code>{id_secteur=value}</code>, <code>{id_secteur==regexp}</code> or <code>{id_secteur IN list}</code> criterion, automatic exclusion process is disabled.

Automatic exclusion can also be disabled by using the <code>{tout_voir}</code> criterion. 
',
	'exclure_sect_slogan' => 'Automatically exclude root sections from SPIP loops',
];
