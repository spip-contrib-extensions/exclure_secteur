<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// Fichier source, a modifier dans https://git.spip.net/spip-contrib-extensions/exclure_secteur.git

return [

	// C
	'configurer_form_titre' => 'Configuration',
	'configurer_menu_entree' => 'Exclure des secteurs',

	// I
	'id_explicite_explication' => 'Ignorer les boucles sur lesquelles l’identifiant de l’objet est explicité ou pris dans le contexte ? <small>(Permet de ne pas modifier certains squelettes comme article.html)</small>',
	'id_explicite_label' => 'Identifiant explicite',

	// R
	'reglages_avances_fieldset' => 'Réglages avancés du plugin',

	// S
	'secteurs_exclus_explication' => 'Choisissez les secteurs à exclure. Ceux-ci n’apparaîtront pas sur le site public, à moins d’utiliser le critère <code>{tout_voir}</code>',
	'secteurs_exclus_fieldset' => 'Secteurs à exclure',

	// T
	'tout_explication' => 'Le critère <code>{tout}</code> est-il équivalent au critère <code>{tout_voir}</code> ?',
	'tout_label' => 'Critère <code>{tout}</code>',
];
