<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/exclure_sect-exclure_secteur?lang_cible=en
// ** ne pas modifier le fichier **

return [

	// C
	'configurer_form_titre' => 'Settings',
	'configurer_menu_entree' => 'Exclude root sections',

	// I
	'id_explicite_explication' => 'Ignore loops on which the object ID is explicit or provided by the context? <small>(Allows you not to modify some templates such as article.html)</small>',
	'id_explicite_label' => 'Explicit ID',

	// R
	'reglages_avances_fieldset' => 'Advanced plugin settings',

	// S
	'secteurs_exclus_explication' => 'Select the root sections you wish to exclude. These will not be displayed on the website, unless you use the <code>{all_see}</code> criterion.',
	'secteurs_exclus_fieldset' => 'Root sections to exclude',

	// T
	'tout_explication' => 'Consider <code>{tout}</code> criterion to be equivalent to <code>{tout_voir}</code> criterion.',
	'tout_label' => 'Criterion <code>{tout}</code>',
];
